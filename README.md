# Zhifu

智富：智能扶贫，精准致富。

# Development
## Prerequisite
An IDE with Node: https://nodejs.org/en/download/  
Garnach  https://truffleframework.com/ganache  
Within project root directory:  
```javascript 
npm install
```
## Web App test
```javascript 
npm test
```
## Smart Contract test

```javascript 
truffle test
```
Windows:
```javascript 
truffle.cmd test
```