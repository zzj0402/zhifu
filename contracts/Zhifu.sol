pragma solidity ^0.4.20;

contract Zhifu {
    //Target has with ID, Name, Income Level, Issues, Solutions
    struct Target {
        uint id;
        string name;
        uint incomeLevel;
        mapping(uint => string) issues;
        mapping(uint => string) solutions;
    }
    mapping(uint => string) targets;
    uint public targetCount;
}
