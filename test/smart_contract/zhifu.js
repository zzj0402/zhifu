var Zhifu = artifacts.require("./Zhifu.sol");

contract("Zhifu", accounts => {
    var zhifuInstance
    it("initializes with Targets.", () => {
        return Zhifu.deployed().then((instance) => {
            return instance.targetCount();
        }).then((count) => {
            assert.equal(count, 0);
        });
    })

}) 